class Joueur():
    """
    A class that

    ...

    Attributes
    ----------


    Methods
    -------


    """

    def __init__(self,nom, sous, actions):
        """Constructor for Joueur"""
        self.nom = nom
        self.pv = 0
        self.sous = sous
        self.listeChantier = []
        self.listeTravailleur = []
        self.listeOutil = []
        self.actions = actions

    def getNom(self):
        return self.nom

    def getSous(self):
        return self.sous
    def addSous(self, val):
        self.sous += val

    def redSous(self, val):
        self.sous -= val

    def setActions(self, val):
        self.actions = val

    def redActions(self, val):
        self.actions -= val

    def getActions(self):
        return self.actions

    def ajouterChantier(self, chantier):
        self.listeChantier.append(chantier)

    def getChantier(self):
        return self.listeChantier

    def getTravailleurs(self):
        return self.listeTravailleur

    def getTravailleursDispo(self):
        res = []
        for tr in self.getTravailleurs():
            if tr.estDispo():
                res.append(tr)
        return res

    def getOutils(self):
        return self.listeOutil

    def ajouterTravailleur(self, trav):
        self.listeTravailleur.append(trav)

    def ajouterOutil(self, outil):
        self.listeOutil.append(outil)

    def getPV(self):
        return self.pv

    def addPV(self, val):
        self.pv += val

    def __str__(self)-> str:
        """__str__ for Joueur"""
        return ''


