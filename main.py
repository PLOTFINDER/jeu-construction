#!/usr/bin/env python

import os
import time
from ouvrier import *
from affranchi import *
from categorie import *
from batiment import *
from machine import *
from ressource import *
from loadCSV import *
from outil import *
from joueur import *
from chantier import *
from bcolors import bcolors


listeCateg = {}
listeBatiments = []
listeRessources = {}
listeAffr = []
listeOutil = []
listeJoueur = []
listeOuvriers = []
listeMachines = []

PRIX_ECHANGE_A_S = 8
ACTIONS_PAR_JOUEUR = 3
SESTERS_DEBUT = 10




def setup():



    categ = loadCSV("data/categorie.csv")
    affranchies = loadCSV("data/affranchi.csv")
    ouvriers = loadCSV("data/ouvrier.csv")
    batiments = loadCSV("data/batiment.csv")
    machines = loadCSV("data/machine.csv")
    ressources = loadCSV("data/ressources.csv")
    outils = loadCSV("data/outils.csv")

    for res in ressources:
        listeRessources[res[0]] = (Ressource(res[0]))

    for outil in outils:
        nom = outil[0]
        stone = outil[1]
        wood = outil[2]
        dye = outil[3]
        bp = outil[4]
        price = outil[5]

        listeOutil.append(Outil(nom, listeRessources, [stone, wood, dye, bp], price))



    for cat in categ:
        nom = cat[0]
        prix = cat[1]

        listeCateg[nom] = (Categorie(nom, prix))

    for ouv in ouvriers:
        nom = ouv[0]
        stone = ouv[1]
        wood = ouv[2]
        dye = ouv[3]
        bp = ouv[4]
        ress = [stone, wood, dye, bp]

        listeOuvriers.append(Ouvrier(nom,listeRessources, ress))

    for affr in affranchies:
        nom = affr[0]
        stone = affr[1]
        wood = affr[2]
        dye = affr[3]
        bp = affr[4]
        ress = [stone, wood, dye, bp]
        cat = Categorie.getInstanceByName(listeCateg, affr[5])
        listeAffr.append(Affranchi(nom,cat,listeRessources,ress))

    for bat in batiments:
        nom = bat[0]
        stone = bat[1]
        wood = bat[2]
        dye = bat[3]
        bp = bat[4]
        pv = bat[5]
        prix = bat[6]
        ress = [stone, wood, dye, bp]

        listeBatiments.append(Batiment(nom, listeRessources, ress, prix, pv))

    for mac in machines:
        nom = mac[0]
        stone = mac[1]
        wood = mac[2]
        dye = mac[3]
        bp = mac[4]
        pv = mac[5]
        prix = mac[6]
        res = mac[7]
        quant = mac[8]
        ress = [stone, wood, dye, bp]

        listeMachines.append(Machine(nom, listeRessources, ress, prix, pv,res,quant))


    listeJoueur.append(Joueur("Anas", SESTERS_DEBUT, ACTIONS_PAR_JOUEUR))
    listeJoueur.append(Joueur("Futulu", SESTERS_DEBUT, ACTIONS_PAR_JOUEUR))

    print(f"{bcolors.FAIL}##################################################################{bcolors.ENDC}")


cls = lambda: os.system('cls' if os.name == 'nt' else 'clear')

def game():
    for joueur in listeJoueur:
        if len(joueur.getChantier())>0:
            count = 0
            chantiers = joueur.getChantier()
            for chantier in chantiers:
                if chantier.fini():
                    bat = chantier.getBatiment()
                    joueur.addPV(bat.getPoints())
                    joueur.addSous(bat.getPrix())

                    count +=1
            print(f"\n{bcolors.HEADER}{count}{bcolors.ENDC} chantiers sont finies")



        joueur.setActions(ACTIONS_PAR_JOUEUR)
        while joueur.getActions() !=0:
            print(f"\nJoueur : {bcolors.HEADER}{joueur.nom}{bcolors.ENDC}")
            print(f"Vous avez {bcolors.HEADER}{len([chantier for chantier in joueur.listeChantier if not chantier.getStatut()])}{bcolors.ENDC} chantiers.")
            print(f"Il vous reste {bcolors.HEADER}{joueur.getActions()}{bcolors.ENDC} actions.")
            print(f"Vous avez {bcolors.HEADER}{joueur.getSous()}{bcolors.ENDC} sesterces.")
            print(f"Vous avez {bcolors.HEADER}{joueur.getPV()}{bcolors.ENDC} points de victoire.")
            choix = None
            while choix not in [1, 2, 3, 4, 5, 6, 7, 8]:
                print(f"{bcolors.OKCYAN}1{bcolors.ENDC} : Ouvrir chantier\n"
                      f"{bcolors.OKCYAN}2{bcolors.ENDC} : Afficher mes chantiers\n"
                      f"{bcolors.OKCYAN}3{bcolors.ENDC} : Recruter un ouvrier\n"
                      f"{bcolors.OKCYAN}4{bcolors.ENDC} : Afficher mes ouvriers\n"
                      f"{bcolors.OKCYAN}5{bcolors.ENDC} : Envoyer un ouvrier travailler\n"
                      f"{bcolors.OKCYAN}6{bcolors.ENDC} : Acheter un outil\n"
                      f"{bcolors.OKCYAN}7{bcolors.ENDC} : Afficher mes outils\n"
                      f"{bcolors.OKCYAN}8{bcolors.ENDC} : Ajouter outil à un Affranchie\n"
                      f"{bcolors.OKCYAN}9{bcolors.ENDC} : Echanger action contre sesterces")
                choix = int(input(f"Choisissez l'{bcolors.OKCYAN}action{bcolors.ENDC} : "))

            if choix == 1:
                ouvrirChantier(joueur)
            elif choix == 2:
                afficherChantier(joueur)
            elif choix == 3:
                recruterOuvrier(joueur)
            elif choix == 4:
                afficherOuvriers(joueur)
            elif choix == 5:
                envoyerOuvrier(joueur)
            elif choix == 6:
                acheterOutil(joueur)
            elif choix == 7:
                afficherOutils(joueur)
            elif choix == 8:
                ajouterOutil(joueur)
            elif choix == 9:
                acheterSesters(joueur)


def ouvrirChantier(joueur):
    cls()
    count = 1
    for bat in listeBatiments:
        print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(bat))
        count += 1

    for mac in listeMachines:
        print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(mac))
        count += 1

    choix = None
    while choix not in [i for i in range(len(listeBatiments)+len(listeMachines)+2)]:
        choix = int(input(f"Choisissez le {bcolors.OKCYAN}batiment{bcolors.ENDC} (0 to exit): "))
    if choix != 0:
        if choix >= len(listeBatiments) + 1:
            bat = listeMachines[choix - len(listeBatiments) - 1]
        else:
            bat = listeBatiments[choix-1]
        joueur.redActions(1)
        joueur.ajouterChantier(Chantier(bat))


def afficherChantier(joueur):
    chantiers = joueur.getChantier()
    if len(chantiers) == 0:
        print(f"\n{bcolors.FAIL}Pas de chantiers{bcolors.ENDC}")
    else:
        cls()
        for chantier in chantiers:
            print("")
            print(chantier)
    input("Press Enter to continue...")

def recruterOuvrier(joueur):
    cls()
    count = 1
    for aff in listeAffr:
        print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(aff))
        count += 1
    for ouvr in listeOuvriers:
        print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(ouvr))
        count += 1

    choix = None
    while choix not in [i for i in range(len(listeAffr)+len(listeOuvriers)+2)]:
        choix = int(input(f"Choisissez le {bcolors.OKCYAN}travailleur (0 to exit){bcolors.ENDC} : "))

    if choix >= len(listeAffr)+1:
        ouvr = listeOuvriers[choix-len(listeAffr)-1]
    else:
        ouvr = listeAffr[choix-1]

    if choix != 0:
        joueur.redActions(1)
        joueur.ajouterTravailleur(ouvr)

def afficherOuvriers(joueur):
    ouvriers = joueur.getTravailleurs()
    if len(ouvriers) == 0:
        print(f"\n{bcolors.FAIL}Pas de travailleurs{bcolors.ENDC}")
    else:
        cls()
        for ouvrier in ouvriers:
            print(ouvrier)
    input("Press Enter to continue...")

def envoyerOuvrier(joueur):
    print("\n")
    if len(joueur.getTravailleursDispo()) == 0:
        print(f"\n{bcolors.FAIL}Pas de travailleurs disponibles{bcolors.ENDC}")
        input("Press Enter to continue...")
    else:
        if len(joueur.getChantier()) == 0:
            print(f"\n{bcolors.FAIL}Pas de chantiers disponibles{bcolors.ENDC}")
            input("Press Enter to continue...")
        else:
            chantiers = joueur.getChantier()
            travailleurs = joueur.getTravailleursDispo()

            count = 0
            for chantier in chantiers:
                print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(chantier))
                count += 1

            choix = None
            while choix not in [i for i in range(len(chantiers))]:
                choix = int(input(f"Choisissez le {bcolors.OKCYAN}chantier{bcolors.ENDC} : "))

            chantierChoisie = chantiers[choix]
            print("\n")
            count = 0
            for trav in travailleurs:
                print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(trav))
                count += 1

            choix = None
            while choix not in [i for i in range(len(travailleurs))]:
                choix = int(input(f"Choisissez le {bcolors.OKCYAN}travailleurs{bcolors.ENDC}  a affecter au chantier de {bcolors.OKBLUE}{chantierChoisie.getBatiment().getNom()}{bcolors.ENDC} : "))

            travChoisie = travailleurs[choix]

            if travChoisie.getPrice() <= joueur.getSous():
                joueur.redSous(travChoisie.getPrice())
                joueur.redActions(1)
                chantierChoisie.ajouterOuvrier(travChoisie)
                travChoisie.setDispo(False)
            else:
                print(f"\n{bcolors.FAIL}Pas assez de sesterces{bcolors.ENDC}")
                input("Press Enter to continue...")



def acheterOutil(joueur):
    cls()
    count = 1
    for outil in listeOutil:
        print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(outil))
        count += 1
    choix = None
    while choix not in [i for i in range(len(listeOutil)+1)]:
        choix = int(input(f"Choisissez l'{bcolors.OKCYAN}outil{bcolors.ENDC} (0 to exit): "))
    if choix != 0:
        outil = listeOutil[choix-1]
        joueur.redActions(1)
        joueur.redSous(outil.getPrix())
        joueur.ajouterOutil(outil)




def afficherOutils(joueur):
    outils = joueur.getOutils()
    if len(outils) == 0:
        print(f"\n{bcolors.FAIL}Pas d'outils{bcolors.ENDC}")
    else:
        cls()
        for outil in outils:
            print(outil)
    input("Press Enter to continue...")

def ajouterOutil(joueur):
    outils = joueur.getOutils()
    if len(outils) == 0:
        print(f"\n{bcolors.FAIL}Pas d'outils{bcolors.ENDC}")
    else:
        ouvr = joueur.getTravailleursDispo()
        if len(ouvr) == 0:
            print(f"\n{bcolors.FAIL}Pas de travailleurs{bcolors.ENDC}")
        else:
            cls()
            affranchies = [affr for affr in ouvr if affr.getPrice() > 0]
            if len(affranchies) == 0:
                print(f"\n{bcolors.FAIL}Pas d'affranchies{bcolors.ENDC}")
            else:
                count = 1
                for affr in affranchies:
                    print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(affr))
                    count += 1

                choix = None
                while choix not in [i for i in range(len(affranchies)+1)]:
                    choix = int(input(
                        f"Choisissez le {bcolors.OKCYAN}travailleur{bcolors.ENDC} (0 to exit): "))

                if choix != 0:
                    affrChoisie = affranchies[choix-1]

                    print("")

                    count = 1
                    for outil in outils:
                        print(f"{bcolors.OKCYAN}{count} : ↓{bcolors.ENDC}\n" + str(outil))
                        count += 1

                    choix = None
                    while choix not in [i for i in range(len(outils) + 1)]:
                        choix = int(input(
                            f"Choisissez l'{bcolors.OKCYAN}outil{bcolors.ENDC} (0 to exit): "))

                    if choix != 0:
                        outilChoisie = outils[choix-1]
                        affrChoisie.ajouterOutil(outilChoisie)
                        joueur.redActions(1)

    input("Press Enter to continue...")







def acheterSesters(joueur):
    print("")
    choix = None
    while choix not in [i for i in range(joueur.getActions()+1)]:
        choix = int(input(f"Vous avez {bcolors.HEADER}{joueur.actions}{bcolors.ENDC} actions\n"
                          f"1 action = {PRIX_ECHANGE_A_S} sesterces\n"
                          f"Choisissez le nombre d'{bcolors.OKCYAN}actions{bcolors.ENDC} a échanger (0 to exit): "))
    if choix !=0:
        joueur.redActions(choix)
        joueur.addSous(choix*PRIX_ECHANGE_A_S)




if __name__ == '__main__':
    setup()
    manches = 0
    while manches != 10:
        game()
        manches += 1
