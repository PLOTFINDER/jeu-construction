import csv

def loadCSV(p_path):
    result = []
    with open(p_path, newline='') as f:
        lines = csv.reader(f, delimiter=',')
        for line in lines: result.append(line)
    return result