#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ouvrier import Ouvrier
from bcolors import bcolors

class Affranchi(Ouvrier):
    """
    A class that creates an affranchi
    
    ...

    Attributes
    ----------
    

    Methods
    -------
    
    
    """

    def __init__(self,nom, categ, dictRess, ress):
        """Constructor for Affranchi"""
        self.__outil = None
        self.__categorie = categ
        self.price = self.__categorie.getPrice()
        super().__init__(nom,dictRess, ress)

    def ajouterOutil(self, outil):
        self.__outil = outil

    def getOutil(self):
        return self.__outil

    def getPrice(self):
        return self.price

    def getCategorie(self):
        """Getter for __categorie from Affranchi class"""
        return self.__categorie

    def getQuantiteTotaleRessources(self):
        res = super().getQuantiteTotaleRessources()
        if self.__outil is not None:
            for prod in res:
                if res[prod].getNom() == self.__outil.getLaRessource().getNom():
                    prod += self.__outil.getQuantite()

        return res

    def getQuantiteParRessource(self, nomRessource):
        res = super().getQuantiteParRessource(nomRessource)
        if self.__outil is not None:
            if self.__outil.getLaRessource().getNom() == nomRessource:
                res+= self.__outil.getQuantite()

        return res


    def __str__(self)-> str:
        """__str__ for Affranchi"""
        return f"═══════════════════════════════════════════════════════════════════\n" \
               f"{bcolors.HEADER}{self.getNom()}{bcolors.ENDC} ({bcolors.OKGREEN}{self.getCategorie()}{bcolors.ENDC})\n" \
               f"{bcolors.OKBLUE}{self.getProductionStr()}{bcolors.ENDC}\n" \
               f"{bcolors.OKBLUE}{self.getPrice()}{bcolors.ENDC} sesters\n" \
               f"{bcolors.OKBLUE}{self.getOutil()}{bcolors.ENDC} 🔧\n" \
               f"═══════════════════════════════════════════════════════════════════\n" \
               f""
