from batiment import Batiment
from bcolors import bcolors

class Machine(Batiment):

    def __init__(self,nom, dictRessources, ress, prix, pv, production , quant):

        self.ressourceProduit = self.setRessourceProduit(dictRessources, production)
        self.quantite = quant
        self.disponible = True
        super().__init__(nom, dictRessources, ress, prix, pv)

    def setRessourceProduit(self, dictRessources, production):
        for res in dictRessources:
            if dictRessources[res].getNom() == production:
                return dictRessources[res]

    def getRessourceProduit(self):
        return self.ressourceProduit

    def getQuantiteProduit(self):
        return self.quantite

    def estDispo(self):
        return self.disponible

    def setDispo(self, val):
        self.disponible = val

    def __str__(self):
        return f"═══════════════════════════════════════════════════════════════════\n" \
               f"{bcolors.HEADER}{self.getNom()}{bcolors.ENDC} \n" \
               f"{bcolors.OKBLUE}{self.getRessourcesStr()}{bcolors.ENDC}\n" \
               f"{bcolors.OKBLUE}{self.getRessourceProduit().getNom()} : {self.getQuantiteProduit()}{bcolors.ENDC}\n" \
               f"{bcolors.OKBLUE}{self.getPoints()}{bcolors.ENDC} points de victoire\n" \
               f"{bcolors.OKBLUE}{self.getPrix()}{bcolors.ENDC} sesters\n" \
               f"═══════════════════════════════════════════════════════════════════\n" \
               f""