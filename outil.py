import random
from bcolors import bcolors
class Outil():
    """
    A class that oui
    
    ...

    Attributes
    ----------
    

    Methods
    -------
    
    
    """

    def __init__(self,nom, dictRess,ress, price):
        """Constructor for Outil"""
        self.nom = nom
        self.__prix = int(price)
        self.__quantite = 1
        self.__ressource = self.setRessources(dictRess, ress)

    def setRessources(self,dictRess, ress):
        for i,k in enumerate(dictRess):
            if ress[i] == '1':
                return dictRess[k]

    def getPrix(self):
        return self.__prix

    def getLaRessource(self):
        return self.__ressource
    
    def getQuantite(self)-> int:
        return self.__quantite
    
    
    def __str__(self)-> str:
        """__str__ for Outil"""
        return f"═══════════════════════════════════════════════════════════════════\n" \
               f"{bcolors.HEADER}{self.nom}{bcolors.ENDC} \n" \
               f"{bcolors.OKBLUE}{self.getLaRessource().getNom()} : {self.getQuantite()}{bcolors.ENDC}\n" \
               f"═══════════════════════════════════════════════════════════════════\n" \
               f""
    
    
