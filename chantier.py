#!/usr/bin/env python
# -*- coding: utf-8 -*-
from bcolors import bcolors
class Chantier():
    """
    A class that oui

    ...

    Attributes
    ----------


    Methods
    -------


    """

    def __init__(self,bat):
        """Constructor for Chantier"""
        self.__batiment = bat
        self.__listeOuvrier = []
        self.__machine = None
        self.prix = 0
        self.setProductionStatut(False)

    def setProductionStatut(self, status):
        self.__statut = status

    def getStatut(self):
        return self.__statut

    def ajouterOuvrier(self, unOuvrier):
        self.__listeOuvrier.append(unOuvrier)

    def ajouterMachine(self, mac):
        self.__machine = mac

    def getMachines(self):
        return self.__machine

    def getOuvrier(self):
        return self.__listeOuvrier

    def getBatiment(self):
        return self.__batiment

    def getRessourcesManquants(self):
        ressourcesNec = dict(self.__batiment.getRessources())
        for lOuvrier in self.__listeOuvrier:
            ressOuvrier = lOuvrier.getQuantiteTotaleRessources()
            dictRessources = self.__batiment.dictRessources
            for uneRessource in dictRessources:
                if ressourcesNec[dictRessources[uneRessource]] > 0:
                    ressourcesNec[dictRessources[uneRessource]] -= ressOuvrier[dictRessources[uneRessource]]


        # ressourcesNec = {res: ressourcesNec[res] for res in ressourcesNec if ressourcesNec[res] > 0}

        ressourcesNecRes = {}
        for res in ressourcesNec:
           if ressourcesNec[res] >= 0:
                ressourcesNecRes[res] = ressourcesNec[res]
           else:
               ressourcesNecRes[res] = 0

        return ressourcesNecRes

    def fini(self):
        count = 0
        ressourcesNec = self.getRessourcesManquants()
        for uneRessource in ressourcesNec:
            if ressourcesNec[uneRessource] <= 0: count += 1

        if count == 4:
            res = True
            self.setProductionStatut(True)

            for ouvr in self.__listeOuvrier:
                ouvr.setDispo(True)


        else:
            res = False

        return res



    def __str__(self)-> str:
        """__str__ for Chantier"""
        ress = self.getRessourcesManquants()
        ress = " ; ".join([" : ".join([prod.getNom(),str(ress[prod])]) for prod in ress])
        return f"{self.getBatiment()}\n" \
               f"{bcolors.OKCYAN}Il manque ↓{bcolors.ENDC}\n" \
               f"{bcolors.OKBLUE}{ress}{bcolors.ENDC}\n" \
               f"══════════════════════════════════════════════════════════════════╝\n"


