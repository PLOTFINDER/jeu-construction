#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ressource import Ressource
from ouvrier import Ouvrier
import random
import math
from bcolors import bcolors


class Batiment:

    def __init__(self, nom, dictRessources, ress, prix, pv):
        self.__pointsV = int(pv)
        self.__nom = nom
        self.dictRessources = dictRessources
        self.prix = int(prix)
        self.__listeRessources = self.setRessources(dictRessources, ress)


    def setRessources(self, dictRessources, ress):
        res = {}
        for i, k in enumerate(dictRessources): res[dictRessources[k]] = int(ress[i])
        return res

    def getRessources(self):
        return self.__listeRessources

    def getNom(self):
        return self.__nom

    def getPoints(self):
        return self.__pointsV

    def getPrix(self):
        return self.prix


    # def getProductionStr(self):
    #     return " ; ".join([" : ".join([prod.getName(),str(self.__listeProduction[prod])]) for prod in self.__listeProduction])
    
    def getRessourcesStr(self):
        return " ; ".join([" : ".join([res.getNom(),str(self.__listeRessources[res])]) for res in self.__listeRessources])


    def __str__(self):
        return f"═══════════════════════════════════════════════════════════════════\n" \
               f"{bcolors.HEADER}{self.getNom()}{bcolors.ENDC} \n" \
               f"{bcolors.OKBLUE}{self.getRessourcesStr()}{bcolors.ENDC}\n" \
               f"{bcolors.OKBLUE}{self.getPoints()}{bcolors.ENDC} points de victoire\n" \
               f"{bcolors.OKBLUE}{self.getPrix()}{bcolors.ENDC} sesters\n" \
               f"═══════════════════════════════════════════════════════════════════\n" \
               f""
