
class Categorie:

    def __init__(self,nomCategorie, price):
        self.__price = int(price)
        self.__name = nomCategorie

    def getName(self):
        return self.__name

    def getPrice(self):
        return self.__price

    @classmethod
    def getInstanceByName(cls, listeInstances, name):
        for inst in listeInstances:
            if listeInstances[inst].getName() == name:
                return listeInstances[inst]

    def __str__(self):
        return self.__name