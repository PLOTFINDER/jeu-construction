#!/usr/bin/env python
# -*- coding: utf-8 -*-
from categorie import *
import math
import random
from ressource import *
from bcolors import bcolors

class Ouvrier:

    def __init__(self,nom , dictRessources, ress):
        self.nom = nom
        self.listeRess = [dictRessources[i] for i in dictRessources]
        self.__production = self.setProduction(dictRessources, ress)
        self.disponible = True

    def getNom(self):
        return self.nom

    def estDispo(self):
        return self.disponible

    def setDispo(self, val):
        self.disponible = val

    def getPrice(self):
        return 0

    def setProduction(self, dictRessources,ress):
        res = {}
        for i,k in enumerate(dictRessources): res[dictRessources[k]] = int(ress[i])
        return res

    def getProduction(self):
        return self.__production

    def getQuantiteTotaleRessources(self):
        return self.getProduction()

    def getQuantiteParRessource(self, nomRessource):
        for ress in self.__production:
            if ress.getName() == nomRessource:
                return self.__production[ress]


    def getProductionStr(self):
        return " ; ".join([" : ".join([prod.getNom(),str(self.__production[prod])]) for prod in self.__production])


    def __str__(self):
        return f"═══════════════════════════════════════════════════════════════════\n" \
               f"{bcolors.HEADER}{self.getNom()}{bcolors.ENDC} \n" \
               f"{bcolors.OKBLUE}{self.getProductionStr()}{bcolors.ENDC}\n" \
               f"═══════════════════════════════════════════════════════════════════\n" \
               f""

